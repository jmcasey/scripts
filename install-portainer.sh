#!/bin/bash

#
# Intended for use on Debian 11 with docker-ce
# Author: jmcasey
# Adapted from portainer-ce installation instructions:
# https://docs.portainer.io/v/ce-2.6/start/install/server/docker/linux
#

mkdir /opt/portainer

systemctl enable docker
systemctl start docker

docker run -d -p 8000:8000 -p 9000:9000 --name=portainer \
	--restart=always \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v /opt/portainer:/data portainer/portainer-ce:2.6.3

