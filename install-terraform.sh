#!/bin/bash

#
# Intended for use on Debian 11
# Author: jmcasey
# Adapted from portainer-ce installation instructions:
# https://learn.hashicorp.com/tutorials/terraform/install-cli
#

apt update
apt install -y \
	gnupg \
	software-properties-common

wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list > /dev/null

apt update
apt install -y \
	terraform