#!/bin/bash

#
# Intended for use on Debian 11
# Author: jmcasey
# Adapted from Docker installation instructions:
# https://docs.docker.com/engine/install/debian/
#

apt update
apt install -y \
	ca-certificates \
	gnupg \
	curl \
	lsb-release

mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/debian/gpg \
	| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
	"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
	 $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update
apt install -y \
 	docker-ce \
 	docker-ce-cli \
 	containerd.io \
 	docker-compose-plugin