#!/bin/bash

#
# Intended for use on Debian 11
# Author: jmcasey
# Adapted from Docker installation instructions:
# https://docs.docker.com/engine/install/debian/
#

apt update
apt upgrade
apt install -y \
	git \
	sudo \
	curl \
	apt-utils

git clone https://gitlab.com/jmcasey/scripts.git
/bin/bash scripts/install-docker.sh
/bin/bash scripts/install-portainer.sh


# Install ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# Install Rust
curl https://sh.rustup.rs -sSf | sh -s -- -y